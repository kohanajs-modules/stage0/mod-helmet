class HelperHelmet {
  static init(app) {
    const { KohanaJS } = require('kohanajs');
    const helmet = require('helmet');

    app.use(helmet.contentSecurityPolicy({
      directives: { ...helmet.contentSecurityPolicy.getDefaultDirectives(), ...KohanaJS.config.helmet.CSP },
    }));
    app.use(helmet.dnsPrefetchControl());
    app.use(helmet.expectCt());
    app.use(helmet.frameguard());
    app.use(helmet.hidePoweredBy());
    app.use(helmet.hsts());
    app.use(helmet.ieNoOpen());
    app.use(helmet.noSniff());
    app.use(helmet.permittedCrossDomainPolicies());
    app.use(helmet.referrerPolicy({ policy: ['strict-origin-when-cross-origin'] }));
    app.use(helmet.xssFilter());
  }
}

module.exports = HelperHelmet;
